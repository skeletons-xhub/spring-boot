# Build
FROM maven:3.6.1-jdk-11-slim AS build
WORKDIR /x-app
COPY pom.xml ./
RUN mvn dependency:go-offline
COPY . ./
RUN mvn clean package

# Copy Build result & start
FROM openjdk:11-slim
RUN useradd -ms /bin/bash xapp
WORKDIR /x-app
COPY --from=build /x-app/target/*.jar /x-app/app.jar

EXPOSE 8080

USER xapp
CMD ["java","-jar", "-Dspring.profiles.active=docker", "/x-app/app.jar"]